<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="style.css">  
  </head>
  <?php
    function printSum($a,$b,$c){
      //sum,ave,min,max
      $sum = $a + $b + $c;
      $ave = $sum/3;

      echo "Sum: ";
      echo $sum;
      echo "<br>";
      echo "Average: ";
      echo $ave;
      echo "<br>";
      echo "Min: ";
      echo min($a,$b,$c);
      echo "<br>";
      echo "Max: ";
      echo max($a,$b,$c);
    }
  ?>

  <script>
    function alertBox(){
      alert("This is an alert");
    }
  </script>
  <header>
    <h1 style = "font-family: Arial">Sample Page</h1>
  </header>  
  
  <body>
    <Table>
      <td>
        <th>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim quod cumque dolore dignissimos quidem quia inventore, <br>
          corporis unde dolorum cum tempora consectetur optio quaerat praesentium sint atque quae aliquid at?
          <br>
          <br>
          <?php
            echo "ARRAY";
            echo "<br>";
            
            $arr = array("the","quick","brown","fox");
            $len = count($arr);


            for($x = 0; $x < $len; $x++){
              echo "<li>";
              echo $arr[$x];
              echo "<br>";
            }
            
            echo "<br>";
            echo printSum(1,2,3);
          ?>

        </th>
        
        
        <th><button class = button onClick = "alertBox()"> Click Me</button></th>
      </td>
    </Table>


    <!-- <div class="grid-container">
      <div class = "grid-item" >
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Error voluptas repudiandae pariatur non praesentium magnam minus ullam voluptatem neque vero? Suscipit fugiat porro asperiores commodi ad? Ipsam maiores sint tempora!</p>
      </div>
      <div class = "grid-item">
        <button class = button> Button</button>
      </div>
    </div>
  </body> -->
  
</html>